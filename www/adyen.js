var exec = require('cordova/exec');

exports.resetSession = (cb) => {  
    cordova.exec(function(retval){
        console.log("resetSession SUCCESS: ", retval)
        cb(null, retval)
    },function(error) {
            console.warn("resetSession FAIL: ", error)
            cb(error, null)
    },'AdyenPlugin', 'resetSession')
}

exports.getInstallationId = (cb) => {
    cordova.exec(function(retval){
        console.log("getInstallationId SUCCESS: ", retval)
        cb(null, retval)
    },function(error) {
            console.warn("getInstallationId FAIL: ", error)
            cb(error, null)
    },'AdyenPlugin', 'getInstallationId')
}

exports.getBatteryLevel = (cb) => {
    cordova.exec(function(retval){
        console.log("getBatteryLevel SUCCESS: ", retval)
        cb(null, retval)
    },function(error) {
            console.warn("getBatteryLevel FAIL: ", error)
            cb(error, null)
    },'AdyenPlugin', 'getBatteryLevel')
}

exports.paymentRequest = (request, txType, cb) => {
    // txType can be either 'cardReader' or 'tapToPay'
    if (typeof txType !== 'string') {txType = 'cardReader'}
    cordova.exec(function(retval){
        console.log("paymentRequest SUCCESS: ", retval)
        cb(null, retval)
    },function(error) {
            console.warn("paymentRequest FAIL: ", error)
            cb(error, null)
    },'AdyenPlugin', 'performTransaction', [request, txType])
}

exports.getDiagnosticsReport = (cb) => {
    // txType can be either 'cardReader' or 'tapToPay'
    if (typeof txType !== 'string') {txType = 'cardReader'}
    cordova.exec(function(retval){
        console.log("getDiagnosticsReport SUCCESS: ", retval)
        cb(null, retval)
    },function(error) {
        console.warn("getDiagnosticsReport FAIL: ", error)
        cb(error, null)
    },'AdyenPlugin', 'getDiagnosticsReport')    
}
exports.warmup = (cb) => {
        
    cordova.exec(function(retval){
            console.log("warmup SUCCESS: ", retval)
            cb(null, retval)
    },function(error) {
            console.warn("warmup FAIL: ", error)
            cb(error, null)
    },'AdyenPlugin', 'warmup')
}
exports.isPassCodeSet = (cb) => {
    cordova.exec(function(retval){
            console.log("isPassCodeSet SUCCESS: ", retval)
            cb(null, retval)
    },function(error) {
            console.warn("isPassCodeSet FAIL: ", error)
            cb(error, null)
    },'AdyenPlugin', 'isPassCodeSet')
}
exports.proximityReaderDiscovery = (cb) => {
    cordova.exec(function(retval){
            console.log("proximityReaderDiscovery SUCCESS: ", retval)
            cb(null, retval)
    },function(error) {
            console.warn("proximityReaderDiscovery FAIL: ", error)
            cb(error, null)
    },'AdyenPlugin', 'proximityReaderDiscovery')
}
exports.warmupPassSdkBlob = (blob, cb) => {
    cordova.exec(function(retval){
        console.log("warmupPassSdkBlob SUCCESS: ", retval)
        cb(null, retval)   
    },function(error) {
            console.warn("warmupPassSdkBlob FAIL: ", error)
            cb(error, null)
    },'AdyenPlugin', 'passSdkBlob', [blob])
}
        
exports.presentDeviceManagement = (cb) => {
    cordova.exec(function(retval){
        console.log("presentDeviceManagement SUCCESS: ", retval)
        cb(null, retval)   
    },function(error) {
            console.warn("presentDeviceManagement FAIL: ", error)
            cb(error, null)
    },'AdyenPlugin', 'presentDeviceManagement')
    
}        

exports.encrypt = (cardNumberValue, expiryYear, expiryMonth, securityCodeValue, cb) => {
    cordova.exec(function(token){
        console.log("SUCCESS: ", token)
        let array = token.split("\r\n")
        let retval = {
            encryptedCardNumber: array[0],
            encryptedExpiryMonth: array[2],
            encryptedExpiryYear: array[1],
            encryptedSecurityCode: array[3]
        }
        cb(null, retval)
    },function(error) {
        console.warn("FAIL: ", error)
        cb(error, null)
    },'AdyenPlugin', 'encryptPaymentData',[ cardNumberValue, expiryYear, expiryMonth, securityCodeValue ])

}
