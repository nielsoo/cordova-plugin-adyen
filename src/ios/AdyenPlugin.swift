import Adyen
import Foundation
import UIKit
import AdyenPOS
import ProximityReader
import LocalAuthentication

struct pubVars {
    static var warmupCallbackId: String!
    static var sdkBlob: String! = ""
    static var registerBusyFiring: Bool = false
    static var setupToken: String! = ""
}


@available(iOS 17.0, *)
class CustomViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white // Set a background color so you can see the view
        // Additional setup...
    }
}

@available(iOS 17.0, *)
@objc(AdyenPlugin)
class AdyenPlugin: CDVPlugin {

    private lazy var paymentService: PaymentService = PaymentService(delegate: self)
    
    var callbackId: String!
    var cardNumberValue: String!
    var expiryYear: String!
    var expiryMonth: String!
    var securityCodeValue: String!
    var PUBLIC_KEY: String!
    
    /* starts the transaction */
    @objc(performTransaction:)
    func performTransaction(command: CDVInvokedUrlCommand) {
        Task {
            await performTransactionAsync(command: command)
        }

    }
    func performTransactionAsync(command: CDVInvokedUrlCommand) async {
        self.callbackId = command.callbackId
        var pluginResult: CDVPluginResult
        do {
            /// Get the payment interface from the payment service
            let txType = command.arguments[1] as! String
            let paymentInterface: AdyenPOS.PaymentInterface

            if (txType == "tapToPay") {
                paymentInterface = try await paymentService.getPaymentInterface(with: .tapToPay)
            } else {
                paymentInterface = try await paymentService.getPaymentInterface(with: .cardReader)
            }
            
            let paymentRequest = command.arguments[0] as! String
            print("PAYMENT REQUEST =")
            print(paymentRequest)
            
//            let data = try JSONSerialization.data(withJSONObject: Data(paymentRequest.utf8))
//            guard let response = try await self.service?.initializeTransaction(
//                viewController: viewController,
//                request: jsonData) else {
//                call.reject(TapToPayError.transactionInitialisationFailed.localizedDescription)
//                return
//            }
//            let data = Data(paymentRequest.utf8)
//            print(data)
            
            let data = Data(paymentRequest.utf8)
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            print("PAYMENT REQUEST AS JSON =")
            print(json)
            let jsonData = try JSONSerialization.data(withJSONObject: json)
            print("PAYMENT REQUEST AS JSONDATA =")
            print(jsonData)
            
            print("create transaction")
            let transaction = try Payment.Request(data: jsonData)
            print("createD transaction")
            print(transaction)
            
                
            let presentationModeParams = try PaymentService.Parameters.init(successScreenTimeout: 3)
            let presentationMode: TransactionPresentationMode = .presentingViewController(
                viewController,
                parameters: presentationModeParams
            )

            /// Perform the transaction using the generated request, payment interface, and view presentation mode
            print("start transaction")
            let response = await paymentService.performTransaction(
                with: transaction,
                paymentInterface: paymentInterface,
                presentationMode: presentationMode
            )
            print("started transaction, got response")
            
            // convert the response to string
            let encoder = JSONEncoder()
            //encoder.outputFormatting = .prettyPrinted
            //let test = try! encoder.encode(response)
            let responseString = String(data: response, encoding: .utf8)!
            print(responseString)

            
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: responseString
            )
            commandDelegate!.send(pluginResult, callbackId: self.callbackId)
            
        } catch {
            print("Unexpected error: \(error).")
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_ERROR,
                messageAs: "Unexpected error: \(error)."
            )
            commandDelegate!.send(pluginResult, callbackId: self.callbackId)
            
        }
        
    }
    
    /* pops up the NYC1 device pairing screen */
    @objc(presentDeviceManagement:)
    func presentDeviceManagement(command: CDVInvokedUrlCommand) {
        self.callbackId = command.callbackId
        var pluginResult: CDVPluginResult
        
        if let currentViewController = self.viewController {
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: "OK"
            )
            commandDelegate!.send(pluginResult, callbackId: self.callbackId)
            
            let deviceManagementViewController = DeviceManagementViewController(paymentService: paymentService)
            currentViewController.present(deviceManagementViewController, animated: true)
        } else {
            print("Unexpected error.")
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_ERROR,
                messageAs: "UNEXPECTED_ERROR"
            )
            commandDelegate!.send(pluginResult, callbackId: self.callbackId)
        }
    }
    
    /* after the register and the setupToken is sent to crispserver we receive an SDK blob from crispserver we pass to the SDK which we return inside the register function */
    @objc(passSdkBlob:)
    func passSdkBlob(command: CDVInvokedUrlCommand) {
        self.callbackId = command.callbackId
        var pluginResult: CDVPluginResult

        pubVars.sdkBlob = command.arguments[0] as! String
        
        pluginResult = CDVPluginResult(
            status: CDVCommandStatus_OK,
            messageAs: "OK"
        )
        commandDelegate!.send(pluginResult, callbackId: self.callbackId)

    }

    /* gets a dignostics report */
  
    @objc(getDiagnosticsReport:)
    func getDiagnosticsReport(command: CDVInvokedUrlCommand) {
      Task {
        await getDiagnosticsReportAsync(command: command)
      }
    }
  
    func getDiagnosticsReportAsync(command: CDVInvokedUrlCommand) async {
      self.callbackId = command.callbackId
      pubVars.warmupCallbackId = self.callbackId
      
      var pluginResult: CDVPluginResult
      do {
        // \(paymentService.installationId)
        let saleToPOIRequest = try "{\"SaleToPOIRequest\":{\"MessageHeader\":{\"ProtocolVersion\":\"3.0\",\"MessageClass\":\"Service\",\"MessageCategory\":\"Diagnosis\",\"MessageType\":\"Request\",\"ServiceID\":\"040\",\"SaleID\":\"POSSystemID12345\",\"POIID\":\"\(paymentService.installationId)\"},\"DiagnosisRequest\":{\"HostDiagnosisFlag\":false}}}"
        
        let data = Data(saleToPOIRequest.utf8)
        let json = try JSONSerialization.jsonObject(with: data, options: [])
        print("SALE TO POI REQUEST AS JSON =")
        print(json)
        let jsonData = try JSONSerialization.data(withJSONObject: json)
        print("SALE TO POI  REQUEST AS JSONDATA =")
        print(jsonData)
        
        let req = try Diagnosis.Request(data: jsonData)
        let response = await paymentService.performDiagnosis(with: req)
        
        let responseString = String(data: response, encoding: .utf8)!
        
        print("DIAGNOSIS RESPONSE: \(responseString)")
        
        pluginResult = CDVPluginResult(
            status: CDVCommandStatus_OK,
            messageAs: responseString
        )
        commandDelegate!.send(pluginResult, callbackId: self.callbackId)
        
      } catch {
        print("Unexpected error in getDiagnosticsReportAsync: \(error).")
        pluginResult = CDVPluginResult(
            status: CDVCommandStatus_ERROR,
            messageAs: "UNEXPECTED_ERROR"
        )
        commandDelegate!.send(pluginResult, callbackId: self.callbackId)

        
      }

    }

    @available(iOS 18.0, *)
    @objc(proximityReaderDiscovery:)
    func proximityReaderDiscovery(command: CDVInvokedUrlCommand) {
        Task {
            await proximityReaderDiscoveryAsync(command: command)
        }
    }
    @available(iOS 18.0, *)
    func proximityReaderDiscoveryAsync(command: CDVInvokedUrlCommand) async {
        self.callbackId = command.callbackId
        
        var pluginResult: CDVPluginResult
        do {
            print("run proximityReaderDiscovery")
            let proximityReaderDiscovery = ProximityReaderDiscovery()

            if let currentViewController = self.viewController {
              pluginResult = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: "OK"
              )
              commandDelegate!.send(pluginResult, callbackId: self.callbackId)
              let content = try await proximityReaderDiscovery.content(for: ProximityReaderDiscovery.Topic.payment(.howToTap))
              try await proximityReaderDiscovery.presentContent(content, from: currentViewController)
              
            } else {
              print("Unexpected error in proximityReaderDiscovery (1)")
              pluginResult = CDVPluginResult(
                status: CDVCommandStatus_ERROR,
                messageAs: "UNEXPECTED_ERROR"
              )
              commandDelegate!.send(pluginResult, callbackId: self.callbackId)
            }
            
        } catch {
            print("Unexpected error: in proximityReaderDiscovery(3) \(error).")
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_ERROR,
                messageAs: "UNEXPECTED_ERROR"
            )
            commandDelegate!.send(pluginResult, callbackId: self.callbackId)
            
        }
        
    }

    /* Warms uo the SDK and the payment session .. if an SDK blob was recently negotiated, the warmup will succeed immediately, if not the wsarmup will call the register() function and a neww SDK blob will have to be requested with the setupToken */
    
    @objc(warmup:)
    func warmup(command: CDVInvokedUrlCommand) {
        Task {
            await warmupAsync(command: command)
        }
    }
    func warmupAsync(command: CDVInvokedUrlCommand) async {
        self.callbackId = command.callbackId
        pubVars.warmupCallbackId = self.callbackId
        
        var pluginResult: CDVPluginResult
        do {
            try print("InstallationID \(paymentService.installationId)")
            
            // for now we always reset the session to get a new sdkBLOb
            //try await paymentService.resetSession()
            if pubVars.registerBusyFiring {
              var pluginResult: CDVPluginResult
              
              pluginResult = CDVPluginResult(
                  status: CDVCommandStatus_OK,
                  messageAs: pubVars.setupToken
              )
              commandDelegate!.send(pluginResult, callbackId: pubVars.warmupCallbackId)

            } else {
              try await paymentService.warmUp()
            }
            
            print("WARMUP SUCCEEDED")
            
            // if paymentService.deviceManager.knownDevices.count > 0 {
            //     print(paymentService.deviceManager.knownDevices[0])
            //     paymentService.deviceManager.connect(to: paymentService.deviceManager.knownDevices[0])
            // }
            
            
            
//            pluginResult = CDVPluginResult(
//                status: CDVCommandStatus_OK,
//                messageAs: "OK"
//            )
//            commandDelegate!.send(pluginResult, callbackId: self.callbackId)
            
        } catch {
            print("Unexpected error: \(error).")
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_ERROR,
                messageAs: "UNEXPECTED_ERROR"
            )
            commandDelegate!.send(pluginResult, callbackId: self.callbackId)
            
        }

    }
    @objc(isPassCodeSet:)
    func isPassCodeSet(command: CDVInvokedUrlCommand) {
        Task {
            await isPassCodeSetAsync(command: command)
        }
    }
    func isPassCodeSetAsync(command: CDVInvokedUrlCommand) async {
        self.callbackId = command.callbackId
        
        var pluginResult: CDVPluginResult
        
        let context = LAContext()
        var error: NSError?
        
        // Check if device can evaluate authentication with passcode (or biometrics)
        let isPasscodeEnabled = context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error)

        var message: String = "1"
        if isPasscodeEnabled {
            print("Passcode is set on the device.")
        } else {
            print("No, a passcode is NOT set on the device")
            message = "0"
        }
        
        pluginResult = CDVPluginResult(
            status: CDVCommandStatus_OK,
            messageAs: message
        )
        commandDelegate!.send(pluginResult, callbackId: self.callbackId)

    }
    
    /* resets the current payment session. When this is called, a subsequent warmup will ALWAYS initiate the register() function and a new SDK blob has to be requested */
    @objc(resetSession:)
    func resetSession(command: CDVInvokedUrlCommand) {
        Task {
            await resetSessionAsync(command: command)
        }
    }
    func resetSessionAsync(command: CDVInvokedUrlCommand) async {
        self.callbackId = command.callbackId
        pubVars.warmupCallbackId = self.callbackId
        
        var pluginResult: CDVPluginResult
        do {
            
            try await paymentService.resetSession()
            
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: "OK"
            )
            commandDelegate!.send(pluginResult, callbackId: self.callbackId)
            
        } catch {
            print("Unexpected error: \(error).")
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_ERROR,
                messageAs: "UNEXPECTED_ERROR"
            )
            commandDelegate!.send(pluginResult, callbackId: self.callbackId)
            
        }
    }
    
    /* returns the connected device's battery level in percentage or "DEVICE_NOT_CONNECTED" */
    @objc(getBatteryLevel:)
    func getBatteryLevel(command: CDVInvokedUrlCommand) {
        Task {
            await getBatteryLevelAsync(command: command)
        }
    }
    
    func getBatteryLevelAsync(command: CDVInvokedUrlCommand) async {
        self.callbackId = command.callbackId
        
        var pluginResult: CDVPluginResult
        do {
            if (paymentService.deviceManager.connectedDevice != nil) {
                try print("batteryLevel \(paymentService.deviceManager.connectedDevice?.batteryCapacity)")
                let cap: String
                cap = try "\(paymentService.deviceManager.connectedDevice?.batteryCapacity ?? 0)"
                let isCharging: String
                isCharging = (paymentService.deviceManager.connectedDevice!.isCharging ? "1" : "0")
                let serial: String
                serial = paymentService.deviceManager.connectedDevice!.serialNumber
                pluginResult = CDVPluginResult(
                    status: CDVCommandStatus_OK,
                    messageAs: "[\"CONNECTED\"," + cap + "," + isCharging + ",\"" + serial + "\"]"
                )
                commandDelegate!.send(pluginResult, callbackId: self.callbackId)

            } else {
                try print("batteryLevel not fetchable, no devices connected!")
                pluginResult = CDVPluginResult(
                    status: CDVCommandStatus_OK,
                    messageAs: "[\"DISCONNECTED\"]"
                )
                commandDelegate!.send(pluginResult, callbackId: self.callbackId)

            }
            
        } catch {
            print("Unexpected error: \(error).")
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_ERROR,
                messageAs: "UNEXPECTED_ERROR"
            )
            commandDelegate!.send(pluginResult, callbackId: self.callbackId)
            
        }
        
    }
    
    
    @objc(getInstallationId:)
    func getInstallationId(command: CDVInvokedUrlCommand) {
        Task {
            await getInstallationIdAsync(command: command)
        }
    }
    
    func getInstallationIdAsync(command: CDVInvokedUrlCommand) async {
        self.callbackId = command.callbackId
        
        var pluginResult: CDVPluginResult
        do {
            try print("InstallationID \(paymentService.installationId)")
            
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: try paymentService.installationId
            )
            commandDelegate!.send(pluginResult, callbackId: self.callbackId)
            
        } catch {
            print("Unexpected error: \(error).")
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_ERROR,
                messageAs: "UNEXPECTED_ERROR"
            )
            commandDelegate!.send(pluginResult, callbackId: self.callbackId)
            
        }
        
    }
    
    @objc(encryptPaymentData:)
    func encryptPaymentData(command: CDVInvokedUrlCommand) {
        self.callbackId = command.callbackId
        var pluginResult: CDVPluginResult

        self.cardNumberValue = command.arguments[0] as! String
        self.expiryYear = command.arguments[1] as! String
        self.expiryMonth = command.arguments[2] as! String
        self.securityCodeValue = command.arguments[3] as! String
        
        // this should not be hard-coded
        self.PUBLIC_KEY = "10001|9B5C06411F3A3479C354E4FB9D4FB91107B91C4F31F75420A8B0AF97F1E9DE70F2B626547FDC282CA8769C8BF9A22DF92BF08290EC85DFE1FC3483AFCFD9978451600D6BB55F588659D22558C5E6197DFC063AC4265AD65D2D2229141A291498EDCFFB9A7F537DFDC3AFC4F593619494E1AA821F61414078BE272917C8E82F64047EC8AE26FCB6AF9CDBF6211D32C6878104ECC4B948B63A3A880A7FC0FD01E9B2FC0C5285626B3F285372CC79C066F92B53506DC8B85388CF0C832DAB7AC7E5133D84B85E9365A048258CB5E384CFE570BA90539F04D187EF55A819AAFD764BB9B732C95EB60C8A5366F0947F1D5EE74138CD3FBFAFD29D965AF21E143E7AB7"

        let cardValidator = CardNumberValidator(isLuhnCheckEnabled: true, isEnteredBrandSupported: true )
        let isCardValid: Bool = cardValidator.isValid(cardNumberValue ?? "")
        //let expiryValidator = CardExpiryDateValidator()
        //let isExpiryValid: Bool = expiryValidator.isValid(expiryDate)
        let securityCodeValidator = CardSecurityCodeValidator()
        let isSecurityCodeValid = securityCodeValidator.isValid(securityCodeValue)
        if isCardValid && isSecurityCodeValid {
            
            let cardObject = Card.init(number: cardNumberValue, securityCode: securityCodeValue, expiryMonth: expiryMonth, expiryYear: expiryYear)
            do {
                let encryptedCard = try CardEncryptor.encrypt(card: cardObject, with: self.PUBLIC_KEY)
                print("encrypted card: \(encryptedCard)")
                pluginResult = CDVPluginResult(
                    status: CDVCommandStatus_OK,
                    messageAs: "\(encryptedCard.number ?? "undefined" )\r\n\(encryptedCard.expiryYear ?? "undefined" )\r\n\(encryptedCard.expiryMonth ?? "undefined" )\r\n\(encryptedCard.securityCode ?? "undefined")"
                )
                commandDelegate!.send(pluginResult, callbackId: self.callbackId)
            } catch {
                // do something
                print("Unexpected error: \(error).")
                pluginResult = CDVPluginResult(
                    status: CDVCommandStatus_ERROR,
                    messageAs: "UNEXPECTED_ERROR"
                )
                commandDelegate!.send(pluginResult, callbackId: self.callbackId)
            }

            
        } else {
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_ERROR,
                messageAs: "INVALID_CARD"
            )
            commandDelegate!.send(pluginResult, callbackId: self.callbackId)

        }
    }
    struct SessionsResponse: Decodable {
        let sdkData: String
    }
    struct PaymentRequestStruct: Decodable {
        let SaleToPOIRequest: Data
    }
    
}

@available(iOS 17.0, *)
extension AdyenPlugin: PaymentServiceDelegate {
    internal func register(with setupToken: String) async throws -> String {
//        guard let url = URL(string: "\(self.hostname)/adyen_tap_to_pay_session") else { return "" }
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.setValue(cookies, forHTTPHeaderField: "Cookie")
//
//        let requestBody = try JSONEncoder().encode([
//            "location_id": self.locationId,
//            "setup_token": setupToken
//        ])
//
//        request.httpBody = requestBody
//
//        let (responseData, _) = try await URLSession.shared.upload(for: request, from: requestBody)
//        let data = try JSONDecoder().decode(SessionsResponse.self, from: responseData)
//
//        print("Tap to pay session created.")
//
//        return data.tap_to_pay_session
        pubVars.registerBusyFiring = true
        pubVars.sdkBlob = ""
        pubVars.setupToken = setupToken
      
        var pluginResult: CDVPluginResult
        
        pluginResult = CDVPluginResult(
            status: CDVCommandStatus_OK,
            messageAs: setupToken
        )
        commandDelegate!.send(pluginResult, callbackId: pubVars.warmupCallbackId)

        while pubVars.sdkBlob == "" {
            try await Task.sleep(nanoseconds: 100000000) // 1 sec = 1 miljard dus 100M = 0.1 s
        }
        print("received SDK BLOB from posapp")
        print(pubVars.sdkBlob)
        
//        let data = try JSONDecoder().decode(SessionsResponse.self, from: pubVars.sdkBlob.data(using: .utf8)!)
//
//        print("Tap to pay session created.")
//        print(data)
//        return data.sdkData
        pubVars.registerBusyFiring = false
        return pubVars.sdkBlob

        
    }
}

