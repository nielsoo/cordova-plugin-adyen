# Cordova Adyen Plugin

Minimal plugin providing access to the Adyen SDK, only supported on iOS.

Added - adyenPOS SDK for supporting tap 2 pay and NYC1 devices

for this the AdyenPOS library has to be fetched manually from github after installing the plugin 
check out https://docs.adyen.com/point-of-sale/ipp-mobile/tap-to-pay/integration-ttp/#add-sdk
Alf and Niels have the access token 

## Using

Install the plugin

    $ ionic cordova plugin https://gitlab.com/nielsoo/cordova-plugin-adyen.git


Usage example

```js
AdyenPlugin.encrypt(card, expyear, expmonth, cvc, name, (token) => console.log(token))

```
